#!/usr/bin/gjs

/*
    Usage: gjs simple.js '/path/to/sample1' '/path/to/sample2' '/path/to/output'
*/

const {Gio, GLib, Gtk, GES, Gst, GstPbutils} = imports.gi;

const App = function (ARGV) {
    this.title = 'Example';
    GLib.set_prgname(this.title);

    Gst.init(null);
    GES.init();
    this.application = new Gtk.Application();
    this.application.connect('activate', () => { this.onActivate(ARGV); });
    this.application.run([]);
};


App.prototype.onActivate = function(ARGV) {

    this.window = new Gtk.ApplicationWindow({ application: this.application,
        default_height: 150,
        default_width: 720,
        window_position: Gtk.WindowPosition.CENTER });
    this.window.show_all();

    const sampleFile1 = Gio.file_new_for_path(ARGV[0]);
    const sampleFile2 = Gio.file_new_for_path(ARGV[1]);
    const outputFile = Gio.file_new_for_path(ARGV[2]);


    const timeline = GES.Timeline.new();
    timeline.add_track(GES.AudioTrack.new());
    this.layer = timeline.append_layer();

    this.pipeline = GES.Pipeline.new();
    this.pipeline.set_timeline(timeline);
    this.pipeline.set_render_settings(outputFile.get_uri(), this._getProfile());
    this.pipeline.set_mode(GES.PipelineFlags.RENDER); // Must be called after set_render_settings!


    this.isAdded = [false, false];
    /* Add your clips to the unique layer while recording then at the end render that timeline: */
    GES.Asset.request_async(GES.UriClip, sampleFile1.get_uri(), null, (_, res) => this._addAsset(0, 0, res));
    GES.Asset.request_async(GES.UriClip, sampleFile2.get_uri(), null, (_, res) => this._addAsset(1, 5 * Gst.SECOND, res));

    
};


App.prototype._addAsset = function(index, startPos, res) {
    const asset = GES.Asset.request_finish(res); // GES.UriClipAsset
    this.layer.add_asset(asset, startPos, 0, asset.get_duration(), GES.TrackType.AUDIO);
    this.isAdded[index] = true;

    if (!this.isAdded.some(v => v === false))
        this.pipeline.set_state(Gst.State.PLAYING);
}


App.prototype._getProfile = function() {
    const profile = {
        containerCaps: 'application/ogg',
        audioCaps: 'audio/x-opus'
    };

    let audioCaps = Gst.Caps.from_string(profile.audioCaps);
    audioCaps.set_value('channels', 2);

    let encodingProfile = GstPbutils.EncodingAudioProfile.new(audioCaps, null, null, 1);
    let containerCaps = Gst.Caps.from_string(profile.containerCaps);
    let containerProfile = GstPbutils.EncodingContainerProfile.new('record', null, containerCaps, null);
    containerProfile.add_profile(encodingProfile);

    return containerProfile;
}


//Run the application
new App(ARGV);